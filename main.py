from typing import final
import telebot
from telebot import types

from telegram_bot import config as cfg
from telegram_bot.database import Planner
from telegram_bot.exceptions import TasksNotExists
from telegram_bot.keybords import keybord

bot = telebot.TeleBot(token=cfg.TOKEN)
planner = Planner()


@bot.message_handler(commands=['start'])
def send_keybord(message, text="Привет, чем я могу помочь?"):
    message = bot.send_message(
        chat_id=message.chat.id,
        text=text,
        reply_markup=keybord,
    )
    
    bot.register_next_step_handler(message, callback)
    

@bot.message_handler(content_types=['text'])
def bad_request(message):
    send_keybord(
        message=message,
        text='Я тебя не понимаю :( \nВоспользуйся кнопками!',
    )


def callback(message):
    if message.text == cfg.ADD_BUTTON:
        message = bot.send_message(
            chat_id=message.chat.id,
            text='Введите задачу:'
        )
        
        bot.register_next_step_handler(message, add_task)
        
    elif message.text == cfg.SHOW_BUTTON:
        show_tasks(message=message)
        
    elif message.text == cfg.DELETE_BUTTON:
        delete_one(message)
        
    elif message.text == cfg.DELETE_ALL_BUTTON:
        delete_all(message=message)
    

def add_task(message):
    planner.add(message)
    bot.send_message(
        chat_id=message.chat.id,
        text="Дело добавлено!",
    )
    send_keybord(message=message, text='Чем еще могу помочь?')
    

def show_tasks(message):
    try:
        tasks = planner.show(message)
        bot.send_message(
            message.chat.id,
            text=tasks,
        )
    except TasksNotExists:
        bot.send_message(
            message.chat.id,
            text="у тебя еще нет задач!"
        )
    finally:
        send_keybord(message=message, text='Чем еще могу помочь?')
        
        
def delete_all(message):
    planner.delete_all(message=message)
    bot.send_message(
        message.chat.id,
        text=f"Все задачи удалены."
    )
    send_keybord(message=message, text='Чем еще могу помочь?')


def delete_one(message):
    try:
        tasks = planner.get_tasks(message)
        keybord_deletion = types.ReplyKeyboardMarkup(row_width=2)
        for task in tasks:
            task_button = types.KeyboardButton(task)
            keybord_deletion.add(task_button)
            
        msg = bot.send_message(
            chat_id=message.chat.id,
            text="Выбери задачу для удаления:",
            reply_markup=keybord_deletion,
        )
        
        bot.register_next_step_handler(msg, delete_choosen)
        
    except TasksNotExists:
        bot.send_message(
            chat_id=message.chat.id,
            text="у тебя еще нет задач!",
        )
    

def delete_choosen(message):
    planner.delete_one(message)
    bot.send_message(
        chat_id=message.chat.id,
        text=f"Задача удалена.",
    )
    send_keybord(message=message, text='Чем еще могу помочь?')
        


if __name__ == "__main__":
    bot.infinity_polling()
