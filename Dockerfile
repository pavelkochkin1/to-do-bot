FROM python:3.8

WORKDIR /bot

COPY ./requirements.txt /app/recuirements.txt

RUN pip install --no-cache-dir --upgrade -r /bot/requirements.txt

COPY . /bot

CMD ["python", "main.py"]