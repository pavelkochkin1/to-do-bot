import sqlite3
from itertools import chain

from telegram_bot.config import DB_NAME
from telegram_bot.exceptions import TasksNotExists


class Planner:
    def __init__(self, db_name: str = DB_NAME) -> None:
        self.db_name = db_name
        self._create_table()
        
    def _create_table(self):
        with sqlite3.connect(self.db_name) as conn:
            query = """
                CREATE TABLE IF NOT EXISTS planner(
                    userid INT,
                    plan TEXT
                )
            """
            conn.execute(query)
            
    def add(self, message):
        with sqlite3.connect(self.db_name) as conn:    
            conn.execute(
                "INSERT INTO planner VALUES(?, ?)",
                (message.chat.id, message.text),
            )
            
    def get_tasks(self, message):
        with sqlite3.connect(self.db_name) as conn:
            cur = conn.cursor()
            query="""
                SELECT plan
                FROM planner
                WHERE userid=(?)
            """
            cur.execute(
                query,
                (message.chat.id, ),
            )
            tasks = cur.fetchall()
            tasks = list(chain.from_iterable(tasks))
            
            if len(tasks) == 0:
                raise TasksNotExists
            
            return tasks
        
    def show(self, message):
        tasks = self.get_tasks(message)
        tasks = [f"{i+1}. {task}" for i, task in enumerate(tasks)]
        return "\n".join(tasks)
    
    def delete_all(self, message):
        with sqlite3.connect(self.db_name) as conn:
            cur = conn.cursor()
            cur.execute(
                "DELETE FROM planner WHERE userid=(?)",
                (message.chat.id, ), 
            )
            conn.commit()
            
    def delete_one(self, message):
        with sqlite3.connect(self.db_name) as conn:
            cur = conn.cursor()
            cur.execute(
                "DELETE FROM planner WHERE (userid, plan)=(?, ?)",
                (message.chat.id, message.text), 
            )
            conn.commit()
