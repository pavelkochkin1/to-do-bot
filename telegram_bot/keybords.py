from telebot import types

from telegram_bot import config as cfg

keybord = types.ReplyKeyboardMarkup()
add_button = types.KeyboardButton(cfg.ADD_BUTTON)
show_button = types.KeyboardButton(cfg.SHOW_BUTTON)
delete_button = types.KeyboardButton(cfg.DELETE_BUTTON)
delete_all_button = types.KeyboardButton(cfg.DELETE_ALL_BUTTON)
keybord.row(add_button, show_button)
keybord.row(delete_button, delete_all_button)